---
title: So here's my first NAS build
author: pokr
date: 2021-10-18 14:10:00 +0800
categories: [Blogging]
tags: [writing]

toc: true
comments: true
---


# The Calamity

It happened again. 

But it happens to most of us. I mean, who hasn't fucked up a Linux install for once? I suppose just that it happened on me 3 or 4 times (as far as I could recall) doesn't prove I'm somehow intellectually inferior, but who knows. 

It has been weeks since the horrible incident but one of the few things I could think of accusing was BTRFS and Docker. (Not really. The only thing to accuse was my own wrongdoing. )

While this combination should not have caused any problems, judging from the number of issues turning up from my countless attempts at Googling to fix my partition, it did on my system when I tried to install `minikube` with horrible network connection. 

> minikube quickly sets up a local Kubernetes cluster on macOS, Linux, and Windows. *[Source](https://minikube.sigs.k8s.io/docs/)*

In theory everything in `minikube` would just work, but not in my case. As Google's docker registry was behind Google-owned domain, I was constantly having troubles initiating a `minikube` install. (It's because I'm living behind [GFW](https://en.wikipedia.org/wiki/Great_Firewall). Of course. ) There were countless attempts at setting up proxy envs in order to pass it into `minikube`'s docker-in-docker, many of which were interrupted brutally out of my pitiful lack of patience, which behind the hood could have left tons of unfinished images / container volumes dangling in `/var/lib/docker/btrfs`. I did not voluntarily opted to use BTRFS [storage driver](https://docs.docker.com/storage/storagedriver/btrfs-driver/) though, but would assume docker picked it automatically when it was installed under a BTRFS root filesystem. 

I could not recall the details, but during one of those sweaty proxy configuration attempts, `minikube` hanged. What followed was a few dirty reboots that very likely had decided the partition's fate, and ultimately, a failed boot with `failed to start flush journal to persistent storage` warning. Various attempts at fixing it up either failed or I had failed at understanding solutions posted by people online, and finally I discovered that on mounting the failed partition in Live USB system, not only a certain `btrfs-cleaner` runs constantly at 100% CPU (1 core), the whole Live USB system would freeze after 20 minutes or so. Any write operation would hang with the corresponding process not responding at 0% CPU. Luckily, read operations are still functioning, so I salvaged what I had to take, and did a reinstall. 

------

However, the above analysis was a mere guess, and was not proved in any way as I promptly hopped into the next install. 

Lesson learned: Never use BTRFS in production with your (rather) precious data without reading through the [BTRFS wiki](https://btrfs.wiki.kernel.org/index.php/Main_Page). What I have been doing was, as a typical Linux newbie, installed Manjaro around a year ago after the last fuckup which happened on a Kubuntu install, thought I wouldn't have another torture like that and got to know `Timeshift`, jumped into BTRFS thinking it'll *just work* (which, it pretty much does, until it doesn't), and handed all subvolume & snapshot controls over to `Timeshift`. It did save my day once or twice, when after a rolling update something had begun to show strange behavior and I decided not to spend another afternoon trying to figure it out. But a BTRFS snapshot is never going to work on a partition level, and I think it's what got me that day. 

![Yeah. Now I understand. ](/home/pokr/.config/Typora/typora-user-images/image-20211022093041317.png)

# The reasonable build

Being a senior student, in order to survive the future torments, I threw myself into an internship. A pitiful amount as it is, the salary was hard-earned all the same. The first month's salary vaporized upon receiving, and turned into a Sony WI-1000XM2. Worth every cent, if you ask me. 

Anyway, the second payroll was very lucky to arrive right after the terrible incident. And when I thought (again) it would not happen again, I decided to go for a NAS box to store my laptop snapshots and replace the Raspberry Pi 4B with a USB external drive mounted, which I used to torrent and run a few Linux based services. Turns out, though, that a USB connection is quite limited in terms of speed and stability. Over a long period of time, the Pi box sustained a "just works" state but always with a few stings in my heart, not knowing what could go wrong some day on this piece of hardware on a thin board, with the whole system resting on an SD card (can be transferred to an SSD, but not quite justifiable). 

As time passed, the drive itself began to complain of bad sectors as well. And that was the final push. I started browsing [Serverbuilds](https://www.serverbuilds.net/) frequently for a NAS build, and settled on a series named NAS Killer 5.0, featuring LGA1150 Intel CPUs. 

It's rather interesting, for the last time I built a PC it was in 2015, and came out with an i7-4790 setup. That was Haswell, on LGA 1150. And I did it again in 2021. 

The rest of the build was the easy part. 


# A farewell to reinstalls (hopefully)

